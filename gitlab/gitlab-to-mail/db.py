from sqlalchemy import create_engine, Column, Integer, Unicode, DateTime, Date
from sqlalchemy.orm import declarative_base, sessionmaker
from util import Settings

import sys

settings = Settings(sys.argv[1])

Base = declarative_base()


class MRVersion(Base):
    __tablename__ = 'MRVersion'

    id = Column(Integer, primary_key=True)
    mr_iid = Column(Integer, nullable=False)
    version = Column(Integer, nullable=False)


class GlobalState(Base):
    __tablename__ = 'GlobalState'

    id = Column(Integer, primary_key=True)
    key = Column(Unicode, index=True, unique=True, nullable=False)
    type = Column(Unicode)
    datetime_value = Column(DateTime)
    date_value = Column(Date)
    integer_value = Column(Integer)


engine = create_engine(f"sqlite:///{settings.DATABASE}", echo=False)
Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
session = Session()


def _get_global(key):
    result = session.query(GlobalState).filter_by(key=key).all()

    if not result:
        return None
    elif len(result) == 1:
        return result[0]

    raise Exception


def get_last_mr_updated_at():
    result = _get_global("last_mr_updated_at")

    if result:
        return result.datetime_value

    return None


def set_last_mr_updated_at(dt):
    gs = _get_global("last_mr_updated_at")
    if gs:
        gs.datetime_value = dt
    else:
        gs = GlobalState(key="last_mr_updated_at", datetime_value=dt)
        session.add(gs)
    session.commit()


def get_last_event_date():
    result = _get_global("last_event_date")

    if result:
        return result.date_value

    return None


def set_last_event_date(date):
    gs = _get_global("last_event_date")
    if gs:
        gs.date_value = date
    else:
        gs = GlobalState(key="last_event_date", date_value=date)
        session.add(gs)
    session.commit()


def get_last_event_id():
    result = _get_global("last_event_id")

    if result:
        return result.integer_value

    return None


def set_last_event_id(integer):
    gs = _get_global("last_event_id")
    if gs:
        gs.integer_value = integer
    else:
        gs = GlobalState(key="last_event_id", integer_value=integer)
        session.add(gs)
    session.commit()


def was_mr_version_processed(mr_iid, version):
    if session.query(MRVersion).filter_by(mr_iid=mr_iid, version=version).all():
        return True
    return False


def mark_mr_version_processed(mr_iid, version):
    mrv = MRVersion(mr_iid=mr_iid, version=version)
    session.add(mrv)
    session.commit()
