#!/bin/bash
# Run through the various steps given to us by GitLab
#  We get a first parameter of a script and a second parameter
#  that is the step name.
set -e
zdir=`dirname "$0"`
source $zdir/config

bname=`basename "$1"`
$VA copy "$1" C:\\$bname
timeout=$TIMEOUT_MISC
if [ "$2" = "build_script" -o "$2" = "step_script" ] ; then
    timeout=$TIMEOUT_BUILD_SCRIPT
fi
$VA run C:\\$bname --timeout $timeout
