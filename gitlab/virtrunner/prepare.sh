#!/bin/bash
# Prepare to run a test by starting our VM.
#  We halt it if it's running, revert it to the desired snapshot,
#  and then set the clock.

set -e
zdir=`dirname "$0"`
source $zdir/config

virsh dominfo $DOMAIN | grep '^State:' | grep -i 'shut off' || virsh destroy $DOMAIN
virsh snapshot-revert $DOMAIN --snapshotname $SNAPSHOT
virsh start $DOMAIN
begin=`date +%s`
now=$begin
while [ $now -lt $((begin + TIMEOUT_MISC)) ] ; do
    $VA sync && break
    now=`date +%s`
    sleep 1
done

set -e
if [ $now -ge $((begin + TIMEOUT_MISC)) ] ; then
    echo "Error: timeout starting VM."
    exit 1
fi

$VA clock ${now}000000000
