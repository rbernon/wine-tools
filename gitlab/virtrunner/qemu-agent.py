#!/usr/bin/env -S python3 -B
""" qemu-agent
        Invoke the virsh qemu-guest-agent command to send and receive
    files and to run commands.
"""
import subprocess
import argparse
import sys
import json
import copy
import base64
import time
import random

def execute(virsh, command, timeout=None):
    """ Execute a command with the qemu guest agent """
    cmd = copy.deepcopy(virsh)
    cmd.append(json.dumps(command))
    try:
        result = subprocess.run(cmd, capture_output=True, timeout=timeout, check=True)
    except subprocess.CalledProcessError as exception:
        print(exception, file=sys.stderr)
        sys.exit(exception.returncode)
    except FileNotFoundError as exception:
        print(exception, file=sys.stderr)
        sys.exit(2)
    return json.loads(result.stdout)


def copyfile(virsh, args):
    """ Copy a file from host to guest """

    # Read the file into a base64 buffer
    try:
        with open(args.source, mode="rb") as handle:
            contents = handle.read()
    except FileNotFoundError as exception:
        print(exception, file=sys.stderr)
        return 2

    # Open the file
    command = {
        "execute": "guest-file-open",
        "arguments": { "path": args.dest, "mode": "wb+" }
        }
    ret = execute(virsh, command)
    if not 'return' in ret:
        print("Error: did not get expected file handle return", file=sys.stderr)
        return 1
    handle = ret['return']

    # An attempt to use getconf ARG_MAX fizzled; there seems to be a max
    #  at around 128K (we use about half because base64)
    maxlen = 64000

    # Put the contents of the file
    while len(contents) > 0:
        if len(contents) > maxlen:
            b64 = base64.b64encode(contents[:maxlen])
        else:
            b64 = base64.b64encode(contents)
        command = {
            "execute": "guest-file-write",
            "arguments": { "handle": handle, "buf-b64": b64.decode("utf-8") }
            }
        ret = execute(virsh, command)
        if not 'return' in ret or not 'count' in ret['return']:
            print("Error: did not get expected count return", file=sys.stderr)
            return 1
        count = int(ret['return']['count'])
        contents = contents[count:]
        if count == 0:
            print("Error: did not write any bytes", file=sys.stderr)
            return 1

    # Close the file
    command = {
        "execute": "guest-file-close",
        "arguments": { "handle": handle }
        }
    execute(virsh, command)
    return 0

def run(virsh, args):
    """ Run a command inside powershell """
    command = {
        "execute": "guest-exec",
        "arguments": { "path": "powershell.exe", "capture-output": True,
                       "arg": [ "-ExecutionPolicy", "Bypass", args.command ]
                     }
        }

    ret = execute(virsh, command)
    if not 'return' in ret or not 'pid' in ret['return']:
        print("Error: did not get expected process handle return", file=sys.stderr)
        sys.exit(1)
    pid = int(ret['return']['pid'])

    now = time.monotonic()
    while time.monotonic() < now + args.timeout:
        time.sleep(0.1)
        command = {
            "execute": "guest-exec-status", "arguments": { "pid": pid }
            }
        ret = execute(virsh, command)
        if 'return' in ret and 'exited' in ret['return'] and ret['return']['exited']:
            if 'err-data' in ret['return']:
                data = base64.b64decode(ret['return']['err-data'])
                print(data.decode("utf-8"), file=sys.stderr)
            if 'out-data' in ret['return']:
                data = base64.b64decode(ret['return']['out-data'])
                print(data.decode("utf-8"))
            # You can get an exit with a 'signalled' state that does not provide an exitcode
            if 'exitcode' in ret['return']:
                return ret['return']['exitcode']
            return(-1)

    print("Command {} did not complete in {}".format(args.command, args.timeout), file=sys.stderr)
    return 1

def setclock(virsh, args):
    """ Set the time in nanoseconds since 1970 """
    command = {
        "execute": "guest-set-time",
        "arguments": { "time": int(args.nanoseconds) }
        }
    ret = execute(virsh, command)
    if 'return' not in ret:
        print("clock set failed.", file=sys.stderr)
        print(ret, file=sys.stderr)
        return 1
    return 0

def sync(virsh):
    """ Make sure we are synced with the guest agent """
    syncid = random.randrange(1000000)
    command = {
        "execute": "guest-sync",
        "arguments": { "id": syncid }
        }
    ret = execute(virsh, command)
    if not 'return' in ret or ret['return'] != syncid:
        print("sync failed.", file=sys.stderr)
        print(ret, file=sys.stderr)
        return 1
    return 0


def main():
    """ Parse arguments, execute command"""
    parser = argparse.ArgumentParser()
    parser.add_argument("--connect", help="Connect to the specified URI")
    parser.add_argument("--domain", help="Execute command on given domain")
    subparsers = parser.add_subparsers(help="Choose an option", dest="option")
    parser_copy = subparsers.add_parser('copy',
                    help="Copy files to guest (slowly; suitable for small transfers)")
    parser_copy.add_argument("source", help="Source filename")
    parser_copy.add_argument("dest", help="Destination filename")
    parser_run = subparsers.add_parser('run', help="Run a command on the guest inside powershell")
    parser_run.add_argument("command", help="Command to run")
    parser_run.add_argument("--timeout", help="Timeout in seconds", default=60, type=float)
    parser_clock = subparsers.add_parser('clock', help="Set the clock")
    parser_clock.add_argument("nanoseconds", help="Nanoseconds UTC from 1970 to send")
    subparsers.add_parser('sync', help="Make sure we the client is up")
    args = parser.parse_args()

    virsh = [ "virsh" ]
    if args.connect:
        virsh.append("--connect")
        virsh.append(args.connect)
    virsh.append("qemu-agent-command")
    if args.domain:
        virsh.append("--domain")
        virsh.append(args.domain)

    if args.option == "copy":
        ret = copyfile(virsh, args)
    elif args.option == "run":
        ret = run(virsh, args)
    elif args.option == "clock":
        ret = setclock(virsh, args)
    elif args.option == "sync":
        ret = sync(virsh)
    else:
        print("Error: you must specify a command to run", file=sys.stderr)
        ret = 1
    sys.exit(ret)

if __name__ == "__main__":
    main()
