# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
# Copyright 2009 Ge van Geldorp
# Copyright 2012, 2014, 2018 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

package ObjectModel::DBIBackEnd;

=head1 NAME

ObjectModel::DBIBackEnd - A storage backend implemented using DBI

=head1 SEE ALSO

ObjectModel::BackEnd

=cut

use Exporter 'import';
use ObjectModel::BackEnd;
our @ISA = qw(ObjectModel::BackEnd);
our @EXPORT = qw(UseDBIBackEnd);

use DBI;
use Time::Local;


sub GetDb($)
{
  my ($self) = @_;

  if (!$self->{Db} or !$self->{Db}->ping())
  {
    # We don't have a connection yet, or it no longer works (probably due to
    # the database idle timeout).
    $self->{Db} = undef;
    while (1)
    {
      # Protect this call so we can retry in case RaiseError is set
      eval { $self->{Db} = DBI->connect(@{$self->{ConnectArgs}}) };
      last if ($self->{Db});

      # Prints errors on stderr like DBI normally does
      $@ ||= "DBI::connect() returned undef without setting an error";
      print STDERR "$@\n";
      sleep(30);
    }
  }
  return $self->{Db};
}

=pod
=over 12

=item C<ToDb()>

Convert the property value to a format suitable for the database.
This is only needed for boolean and timestamp properties.

=back
=cut

sub ToDb($$$)
{
  my ($self, $Value, $PropertyDescriptor) = @_;

  if ($PropertyDescriptor->GetClass eq "Basic")
  {
    if ($PropertyDescriptor->GetType() eq "B")
    {
      if ($Value)
      {
        $Value = "Y";
      }
      else
      {
        $Value = "N";
      }
    }
    elsif ($PropertyDescriptor->GetType() eq "DT")
    {
      if (defined($Value))
      {
        if ($Value == 0)
        {
          $Value = undef;
        }
        else
        {
          my ($Sec, $Min, $Hour, $MDay, $Mon, $Year, $WDay, $YDay, $IsDst) = gmtime($Value);
          $Value = sprintf "%04d-%02d-%02d %02d:%02d:%02d",
                           $Year + 1900, $Mon + 1, $MDay, $Hour, $Min, $Sec;
        }
      }
    }
  }

  return $Value;
}

=pod
=over 12

=item C<FromDb()>

Convert the database values to a format suitable for Perl. See ToDb().

=back
=cut

sub FromDb($$$)
{
  my ($self, $Value, $PropertyDescriptor) = @_;

  if ($PropertyDescriptor->GetClass eq "Basic")
  {
    if ($PropertyDescriptor->GetType() eq "B")
    {
      $Value = ($Value eq "Y");
    }
    elsif ($PropertyDescriptor->GetType() eq "DT")
    {
      if (defined($Value))
      {
        if ($Value eq "0000-00-00 00:00:00")
        {
          $Value = undef;
        }
        else
        {
          my ($Year, $Month, $Day, $Hour, $Min, $Sec) =
            ($Value =~ m/^(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)$/);
          $Value = timegm($Sec, $Min, $Hour, $Day, $Month - 1, $Year - 1900);
        }
      }
    }
  }

  return $Value;
}

=pod
=over 12

=item C<BuildKeyWhere()>

Augments the specified WHERE clause with statements to match the key columns.
It is up to the caller to then append the corresponding key components to the
query's data array.

=back
=cut

sub BuildKeyWhere($$$)
{
  my ($self, $PropertyDescriptors, $Where) = @_;

  # Faster than join+map+grep
  my $PartCount;
  foreach my $PropertyDescriptor (@{$PropertyDescriptors})
  {
    next if (!$PropertyDescriptor->GetIsKey());
    foreach my $ColName (@{$PropertyDescriptor->GetColNames()})
    {
      $Where .= " AND " if ($Where ne "");
      $Where .= "$ColName = ?";
      $PartCount++;
    }
  }
  return ($PartCount, $Where);
}

=pod
=over 12

=item C<BuildFieldList()>

Build the comma-separated SELECT list of columns to load from the database.
Note that only the non-master columns are loaded.

=back
=cut

sub BuildFieldList($$)
{
  my ($self, $PropertyDescriptors) = @_;

  my $Fields = "";
  foreach my $PropertyDescriptor (@$PropertyDescriptors)
  {
    foreach my $ColName (@{$PropertyDescriptor->GetColNames()})
    {
      if ($Fields ne "")
      {
        $Fields .= ", ";
      }
      $Fields .= $ColName;
    }
  }

  return $Fields;
}

=pod
=over 12

=item C<GetFilterWhere()>

Builds the WHERE clause and its associated data parameter from the specified
collection and filter tree.

Returns a triplet composed of a boolean which is true if the returned WHERE
clause is composite, that is if it should be enclosed in parentheses before
being combined with other filters ; the WHERE clause string ; and a reference
to the data array.

=back
=cut

sub GetFilterWhere($$$);
sub GetFilterWhere($$$)
{
  my ($self, $Collection, $Filter) = @_;

  if ($Filter->{Type} eq 'value')
  {
    my $Values = $Filter->{Values};
    if (!$Values or @$Values == 0)
    {
      die "no values for the $Filter->{Property} $Filter->{Comparator} property filter";
    }

    my $PropertyDescriptor = $Collection->GetPropertyDescriptorByName($Filter->{Property});
    my (@Wheres, @Data);
    foreach my $ColValue (@$Values)
    {
      my $ColName = $PropertyDescriptor->GetColNames()->[0];
      push @Wheres, "$ColName $Filter->{Comparator} ?";
      push @Data, $self->ToDb($ColValue, $PropertyDescriptor);
    }
    return (@$Values > 1, join(" OR ", @Wheres), \@Data);
  }

  if ($Filter->{Type} eq 'and' or $Filter->{Type} eq 'or')
  {
    my $Terms = $Filter->{Terms};
    if (!$Terms or !@$Terms)
    {
      die "no terms for the $Filter->{Type} filter";
    }
    if (@$Terms == 1)
    {
      return GetFilterWhere($self, $Collection, $Terms->[0]);
    }

    my (@Wheres, @Data);
    foreach my $Term (@$Terms)
    {
      my ($Composite, $TermWhere, $TermData) = GetFilterWhere($self, $Collection, $Term);
      push @Wheres, ($Composite ? "($TermWhere)" : $TermWhere);
      push @Data, @$TermData;
    }

    my $Operator = ($Filter->{Type} eq 'and' ? " AND " : " OR ");
    return (1, join($Operator, @Wheres), \@Data);
  }

  if ($Filter->{Type} eq 'null' or $Filter->{Type} eq 'not null')
  {
    my $PropertyDescriptor = $Collection->GetPropertyDescriptorByName($Filter->{Property});
    my $ColName = $PropertyDescriptor->GetColNames()->[0];
    my $Operator = ($Filter->{Type} eq 'null' ? "IS NULL" : "IS NOT NULL");
    return (0, "$ColName $Operator", []);
  }

  if ($Filter->{Type} eq 'not')
  {
    if (!$Filter->{Term})
    {
      die "no term for the not filter";
    }
    my ($Composite, $TermWhere, $TermData) = GetFilterWhere($self, $Collection, $Filter->{Term});
    # To avoid confusion add parentheses even it the term is not composite
    return (0, "NOT ($TermWhere)", $TermData);
  }

  die "unsupported '$Filter->{Type}' filter type";
}

=pod
=over 12

=item C<LoadCollection()>

Loads the collection from the corresponding database table, filtering it on
both the master columns and the collection's filter if any.

Note that only the non-master columns are loaded (see BuildFieldList()).

=back
=cut

sub LoadCollection($$)
{
  my ($self, $Collection) = @_;

  my $Fields = $self->BuildFieldList($Collection->GetPropertyDescriptors());

  my $Where = "";
  my @Data;
  my ($MasterColNames, $MasterColValues) = $Collection->GetMasterCols();
  if (defined($MasterColNames))
  {
    $Where = join(" = ? AND ", @{$MasterColNames}) . " = ?";
    push @Data, @{$MasterColValues};
  }

  my $Filter = $Collection->GetFilter();
  if ($Filter)
  {
    my ($Composite, $FilterWhere, $FilterData) = GetFilterWhere($self, $Collection, $Filter);
    if ($Where ne "")
    {
      $Where .= ($Composite ? " AND ($FilterWhere)" : " AND $FilterWhere");
      push @Data, @$FilterData;
    }
    else
    {
      $Where = $FilterWhere;
      @Data = @$FilterData;
    }
  }

  my $Query = "SELECT $Fields FROM " . $Collection->GetTableName();
  if ($Where ne "")
  {
    $Query .= " WHERE $Where";
  }
  my $Statement = $self->GetDb()->prepare($Query);
  $Statement->execute(@Data);

  while (my $Row = $Statement->fetchrow_hashref())
  {
    my $Item = $Collection->CreateItem();
    foreach my $PropertyDescriptor (@{$Collection->GetPropertyDescriptors()})
    {
      foreach my $ColName (@{$PropertyDescriptor->GetColNames()})
      {
        $Item->PutColValue($ColName, $self->FromDb($Row->{$ColName},
                                                   $PropertyDescriptor));
      }
    }
    $Item->ResetModified();

    my $Key = $Item->GetKey();
    $Collection->{Items}{$Key} = $Collection->GetScopeItem($Key, $Item);
  }

  $Statement->finish();
}

=pod
=over 12

=item C<LoadItem()>

Loads the specified Item from the database and adds it to the Collection.
By design this method will not check if the Item is already present in the
Collection scope. Such checks belong in the higher levels. This method will
however put the Item in the Collection's scope.

Note that only the non-master columns are loaded (see BuildFieldList()).

=back
=cut

sub LoadItem($$$)
{
  my ($self, $Collection, $RequestedKey) = @_;

  my $Fields = $self->BuildFieldList($Collection->GetPropertyDescriptors());

  my $Where = "";
  my @Data;
  my ($MasterColNames, $MasterColValues) = $Collection->GetMasterCols();
  if (defined($MasterColNames))
  {
    $Where = join(" = ? AND ", @{$MasterColNames}) . " = ?";
    push @Data, @{$MasterColValues};
  }
  (my $PartCount, $Where) = $self->BuildKeyWhere($Collection->GetPropertyDescriptors(), $Where);
  my @KeyParts = $Collection->SplitKey($RequestedKey);
  return undef if ($PartCount != @KeyParts);
  push @Data, @KeyParts;

  my $Query = "SELECT $Fields FROM " . $Collection->GetTableName();
  if ($Where ne "")
  {
    $Query .= " WHERE $Where";
  }
  my $Statement = $self->GetDb()->prepare($Query);
  $Statement->execute(@Data);

  my $Item;
  if (my $Row = $Statement->fetchrow_hashref())
  {
    $Item = $Collection->CreateItem();
    foreach my $PropertyDescriptor (@{$Collection->GetPropertyDescriptors()})
    {
      foreach my $ColName (@{$PropertyDescriptor->GetColNames()})
      {
        $Item->PutColValue($ColName, $self->FromDb($Row->{$ColName},
                                                   $PropertyDescriptor));
      }
    }
    $Item->ResetModified();
    $Item = $Collection->GetScopeItem($RequestedKey, $Item);
  }

  $Statement->finish();

  return $Item;
}

=pod
=over 12

=item C<BuildInsertStatement()>

Builds the SQL statement for saving new Items to the database.
This can be reused multiple times when saving a collection.

=back
=cut

sub BuildInsertStatement($$$$)
{
  my ($self, $TableName, $PropertyDescriptors, $MasterColNames) = @_;

  my $Fields = "";
  my $PlaceHolders = "";
  if (defined($MasterColNames))
  {
    foreach my $ColName (@$MasterColNames)
    {
      if ($Fields ne "")
      {
        $Fields .= ", ";
        $PlaceHolders .= ", ";
      }
      $Fields .= $ColName;
      $PlaceHolders .= "?";
    }
  }

  foreach my $PropertyDescriptor (@{$PropertyDescriptors})
  {
    foreach my $ColName (@{$PropertyDescriptor->GetColNames()})
    {
      if ($Fields ne "")
      {
        $Fields .= ", ";
        $PlaceHolders .= ", ";
      }
      $Fields .= $ColName;
      $PlaceHolders .= "?";
    }
  }

  return "INSERT INTO $TableName ($Fields) VALUES($PlaceHolders)";
}

=pod
=over 12

=item C<GetInsertData()>

Builds a data array suitable for use with the SQL statement returned by
BuildInsertStatement() for the specified Item. This simply includes all
of the object's properties.

=back
=cut

sub GetInsertData($$$)
{
  my ($self, $MasterColValues, $Item) = @_;

  my @Data;
  if (defined($MasterColValues))
  {
    push @Data, @$MasterColValues;
  }

  foreach my $PropertyDescriptor (@{$Item->GetPropertyDescriptors()})
  {
    foreach my $ColName (@{$PropertyDescriptor->GetColNames()})
    {
      push @Data, $self->ToDb($Item->GetColValue($ColName),
                              $PropertyDescriptor);
    }
  }

  return \@Data;
}

=pod
=over 12

=item C<BuildUpdateStatement()>

Builds the SQL statement for saving *modified* Items to the database.
This can be reused multiple times when saving a collection.

=back
=cut

sub BuildUpdateStatement($$$$)
{
  my ($self, $TableName, $PropertyDescriptors, $MasterColNames) = @_;

  my $Fields = "";
  foreach my $PropertyDescriptor (@{$PropertyDescriptors})
  {
    if (! $PropertyDescriptor->GetIsKey())
    {
      foreach my $ColName (@{$PropertyDescriptor->GetColNames()})
      {
        if ($Fields ne "")
        {
          $Fields .= ", ";
        }
        $Fields .= $ColName . " = ?";
      }
    }
  }

  my $Where = "";
  if (defined($MasterColNames))
  {
    $Where = join(" = ? AND ", @{$MasterColNames}) . " = ?";
  }
  (my $_PartCount, $Where) = $self->BuildKeyWhere($PropertyDescriptors, $Where);

  return "UPDATE $TableName SET $Fields WHERE $Where";
}

=pod
=over 12

=item C<GetUpdateData()>

Returns a data array suitable for use with the SQL statement returned by
BuildUpdateStatement() for the specified Item. It is composed of:
* The non-key properties for the SET part of the statement since only those
  could have been modified.
* The key properties for the WHERE part of the statement.

=back
=cut

sub GetUpdateData($$$)
{
  my ($self, $MasterColValues, $Item) = @_;

  my @Data;
  foreach my $PropertyDescriptor (@{$Item->GetPropertyDescriptors()})
  {
    if (! $PropertyDescriptor->GetIsKey())
    {
      foreach my $ColName (@{$PropertyDescriptor->GetColNames()})
      {
        push @Data, $self->ToDb($Item->GetColValue($ColName),
                                $PropertyDescriptor);
      }
    }
  }

  if (defined($MasterColValues))
  {
    push @Data, @$MasterColValues;
  }

  foreach my $PropertyDescriptor (@{$Item->GetPropertyDescriptors()})
  {
    if ($PropertyDescriptor->GetIsKey())
    {
      foreach my $ColName (@{$PropertyDescriptor->GetColNames()})
      {
        push @Data, $self->ToDb($Item->GetColValue($ColName), $PropertyDescriptor);
      }
    }
  }

  return \@Data;
}

sub SaveCollection($$)
{
  my ($self, $Collection) = @_;

  my $Db = $self->GetDb();
  my ($MasterColNames, $MasterColValues) = $Collection->GetMasterCols();
  my $UpdateQuery = $self->BuildUpdateStatement($Collection->GetTableName(),
                                                $Collection->GetPropertyDescriptors(),
                                                $MasterColNames);
  my $UpdateStatement = $Db->prepare($UpdateQuery);

  my $InsertQuery = $self->BuildInsertStatement($Collection->GetTableName(),
                                                $Collection->GetPropertyDescriptors(),
                                                $MasterColNames);
  my $InsertStatement = $Db->prepare($InsertQuery);

  foreach my $Key (@{$Collection->GetKeysNoLoad()})
  {
    my $Item = $Collection->GetItem($Key);
    if ($Item->GetIsNew())
    {
      $InsertStatement->execute(@{$self->GetInsertData($MasterColValues,
                                                       $Item)});

      foreach my $PropertyDescriptor (@{$Collection->{PropertyDescriptors}})
      {
        if ($PropertyDescriptor->GetIsKey() &&
            $PropertyDescriptor->GetClass() eq "Basic" &&
            $PropertyDescriptor->GetType() eq "S")
        {
          my $ColNames = $PropertyDescriptor->GetColNames;
          if (scalar @{$ColNames} != 1)
          {
            die "Sequence property spans multiple columns";
          }

          # No FromDb() conversion needed for integers
          $Item->PutColValue(@{$ColNames}[0], $Db->{'mysql_insertid'});
          $Collection->KeyChanged($Key, $Item->GetKey());
        }
      }

      $Item->OnSaved();
    }
    elsif ($Item->GetIsModified())
    {
      $UpdateStatement->execute(@{$self->GetUpdateData($MasterColValues, $Item)});
      $Item->OnSaved();
    }
  }

  $InsertStatement->finish();
  $UpdateStatement->finish();
}

sub SaveItem($$)
{
  my ($self, $Item) = @_;

  if ($Item->GetIsNew())
  {
    die "Internal error: Need to save new items via collection";
  }
  return if (!$Item->GetIsModified());

  my ($MasterColNames, $MasterColValues) = $Item->GetMasterCols();
  my $Query = $self->BuildUpdateStatement($Item->GetTableName(),
                                          $Item->GetPropertyDescriptors(),
                                          $MasterColNames);
  my $Statement = $self->GetDb()->prepare($Query);
  $Statement->execute(@{$self->GetUpdateData($MasterColValues, $Item)});
  $Statement->finish();
}

sub DeleteItem($$)
{
  my ($self, $Item) = @_;

  my $Where = "";
  my @Data;
  my ($MasterColNames, $MasterColValues) = $Item->GetMasterCols();
  if (defined($MasterColNames))
  {
    $Where = join(" = ? AND ", @{$MasterColNames}) . " = ?";
    push @Data, @{$MasterColValues};
  }
  (my $_PartCount, $Where) = $self->BuildKeyWhere($Item->GetPropertyDescriptors(), $Where);
  push @Data, $Item->GetKeyComponents();

  my $Statement = $self->GetDb()->prepare("DELETE FROM " .
                                          $Item->GetTableName() .
                                          " WHERE " . $Where);
  $Statement->execute(@Data);
  $Statement->finish();

  return undef;
}

sub DeleteAll($$)
{
  my ($self, $Collection) = @_;

  my $Where = "";
  my @Data;
  my ($MasterColNames, $MasterColValues) = $Collection->GetMasterCols();
  if (defined($MasterColNames))
  {
    $Where = join(" = ? AND ", @{$MasterColNames}) . " = ?";
    push @Data, @{$MasterColValues};
  }

  my $Filter = $Collection->GetFilter();
  if ($Filter)
  {
    my ($Composite, $FilterWhere, $FilterData) = GetFilterWhere($self, $Collection, $Filter);
    if ($Where ne "")
    {
      $Where .= ($Composite ? " AND ($FilterWhere)" : " AND $FilterWhere");
      push @Data, @$FilterData;
    }
    else
    {
      $Where = $FilterWhere;
      @Data = @$FilterData;
    }
  }

  my $Query = "DELETE FROM " . $Collection->GetTableName();
  if ($Where ne "")
  {
    $Query .= " WHERE " . $Where;
  }
  my $Statement = $self->GetDb()->prepare($Query);
  $Statement->execute(@Data);
  $Statement->finish();

  return undef;
}

sub Close($)
{
  my ($self) = @_;

  # The connection will be automatically closed. In a child process this
  # will automatically do the right thing thanks to AutoInactiveDestroy.
  $self->{Db} = undef;
}

sub UseDBIBackEnd($$$$$$)
{
  my ($class, $DbSelector, $DbSource, $DbUser, $DbPassword, $DbArgs) = @_;

  # The implementation assumes AutoCommit is on.
  $DbArgs->{AutoCommit} = 1;
  # Make sure AutoInactiveDestroy is set so the database connection is not
  # broken if we fork() (including if it's behind our back).
  $DbArgs->{AutoInactiveDestroy} = 1;

  my $BackEnd = $class->new();
  $BackEnd->{ConnectArgs} = [$DbSource, $DbUser, $DbPassword, $DbArgs];
  AddDBBackEnd($DbSelector, $BackEnd);
}

1;
