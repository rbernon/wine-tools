# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
# Base class for web pages containing a DB bound form
#
# Copyright 2009 Ge van Geldorp
# Copyright 2012-2014, 2017-2018, 2022 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

package ObjectModel::CGI::ItemPage;

=head1 NAME

ObjectModel::CGI::ItemPage - Base class for bound web forms

=cut

use ObjectModel::CGI::FormPage;
our @ISA = qw(ObjectModel::CGI::FormPage);


sub _initialize($$$$)
{
  my ($self, $Request, $RequiredRole, $Collection) = @_;

  $self->{Collection} = $Collection;

  $self->SUPER::_initialize($Request, $RequiredRole, $Collection->GetPropertyDescriptors());

  if (defined($self->GetParam("Key")))
  {
    $self->{Item} = $Collection->GetItem($self->GetParam("Key"));
  }
  else
  {
    $self->{Item} = undef;
  }
  if (! defined($self->{Item}))
  {
    $self->{Item} = $Collection->Add();
  }
}


#
# Property handling
#

sub GetPropertyValue($$)
{
  my ($self, $PropertyDescriptor) = @_;

  my $PropertyName = $PropertyDescriptor->GetName();
  return $self->{Item}->$PropertyName;
}

sub DisplayProperty($$)
{
  my ($self, $PropertyDescriptor) = @_;

  my $Display = $self->SUPER::DisplayProperty($PropertyDescriptor);
  return # Don't show autoincrement fields of new items: they don't have a
         # value yet and cannot be edited anyway so there is no point.
         ($PropertyDescriptor->GetClass() eq "Basic" and
          $PropertyDescriptor->GetType() eq "S" and
          $self->{Item}->GetIsNew()) ? "" :
         # Don't allow editing key attributes of existing items. Furthermore
         # the item is identified in the page title so hide them altogether.
         ($PropertyDescriptor->GetIsKey() and !$self->{Item}->GetIsNew() and
          $Display eq "rw") ? "" :
         $Display;
}


#
# Form page generation
#

sub GetTitle($)
{
  my ($self) = @_;

  return $self->GetParam("Key") if ($self->GetParam("Key"));

  my $Title = $self->{Collection}->GetItemName();
  $Title =~ s/([a-z])([A-Z])/$1 $2/g;
  return "Add $Title";
}

sub GenerateFormStart($)
{
  my ($self) = @_;

  $self->SUPER::GenerateFormStart();

  if (! $self->{Item}->GetIsNew())
  {
      print "<div><input type='hidden' name='Key' value='",
            $self->escapeHTML($self->{Item}->GetKey()), "' /></div>\n";
  }
}


#
# Actions handling
#

sub GetActions($)
{
  my ($self) = @_;

  return $self->{HasRW} ? ["Save", "Cancel"] : [];
}

sub RedirectToParent($)
{
  my ($self) = @_;

  return $self->Redirect($self->{Collection}->GetCollectionName() . "List.pl");
}

sub OnAction($$)
{
  my ($self, $Action) = @_;

  if ($Action eq "Save")
  {
    return !1 if (!$self->Save());
    exit($self->RedirectToParent());
  }
  if ($Action eq "Cancel")
  {
    exit($self->RedirectToParent());
  }

  return $self->SUPER::OnAction($Action);
}


#
# Validating and saving the form content
#

sub SaveProperty($$$)
{
  my ($self, $PropertyDescriptor, $Value) = @_;

  if ($PropertyDescriptor->GetClass() eq "Basic" &&
      $PropertyDescriptor->GetType() eq "B" && $Value)
  {
    $Value = 1;
  }

  my $PropertyName = $PropertyDescriptor->GetName();
  $self->{Item}->$PropertyName($Value);

  return 1;
}

sub Save($)
{
  my ($self) = @_;

  return !1 if (!$self->SUPER::Save());

  my ($_ErrKey, $ErrProperty, $ErrMessage) = $self->{Collection}->Save();
  if (defined $ErrMessage)
  {
    $self->AddError($ErrMessage, $ErrProperty);
    return undef;
  }
  return 1;
}

1;
