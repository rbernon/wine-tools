# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
# Copyright 2022 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;


package WineTestBot::TaskFailure;

=head1 NAME

WineTestBot::TaskFailure - Ties a failure to its task+log matches

=head1 DESCRIPTION

A TaskFailure is created when a WineTestBot::Failure matches a line in a
task's log. It also records whether the matching failures occurred in previous
logs or not. This makes it possible to detect fixed failures.

=cut

use WineTestBot::WineTestBotObjects;
our @ISA = qw(WineTestBot::WineTestBotItem);


sub Compare($$)
{
  my ($self, $B) = @_;

  return $B->Task->Started <=> $self->Task->Started || # newest first by default
         $self->TaskLog cmp $B->TaskLog;
}


package WineTestBot::TaskFailures;

=head1 NAME

WineTestBot::TaskFailures - A TaskFailure collection

=head1 DESCRIPTION

This collection cross references the known failures with the tasks and logs they
matched.

=cut

use Exporter 'import';
use WineTestBot::WineTestBotObjects;
BEGIN
{
  our @ISA = qw(WineTestBot::WineTestBotCollection);
  our @EXPORT = qw(CreateTaskFailures);
}

use ObjectModel::BasicPropertyDescriptor;
use ObjectModel::ItemrefPropertyDescriptor;
use WineTestBot::Tasks;


sub CreateItem($)
{
  my ($self) = @_;

  return WineTestBot::TaskFailure->new($self);
}

my @PropertyDescriptors = (
  # Identifies the task which has matching failures
  CreateBasicPropertyDescriptor("JobId",  "Job id", 1, 1, "N", 10),
  CreateBasicPropertyDescriptor("StepNo", "Step no", 1, 1, "N", 2),
  CreateBasicPropertyDescriptor("TaskNo", "Task", 1, 1, "N", 2),
  CreateItemrefPropertyDescriptor("Task", "Task", 1, \&WineTestBot::Tasks::CreateTasks, ["JobId", "StepNo", "TaskNo"]),

  # and more specifically in which of its logs
  CreateBasicPropertyDescriptor("TaskLog", "Task log",  1,  1, "A", 32),

  # Also store a count of matching new and old failures
  CreateBasicPropertyDescriptor("NewCount", "Count of matching new failures", !1, !1, "N", 10),
  CreateBasicPropertyDescriptor("OldCount", "Count of matching old failures", !1, !1, "N", 10),
);
SetupItemrefColumns(\@PropertyDescriptors);
my @FlatPropertyDescriptors = (
  CreateBasicPropertyDescriptor("FailureId", "Failure id", 1, 1, "N",  10),
  @PropertyDescriptors
);

=pod
=over 12

=item C<CreateTaskFailures()>

Creates a collection of TaskFailure objects.

=back
=cut

sub CreateTaskFailures(;$$)
{
  my ($ScopeObject, $Failure) = @_;
  return WineTestBot::TaskFailures->new("TaskFailures",
      "TaskFailures", "TaskFailure",
      $Failure ? \@PropertyDescriptors : \@FlatPropertyDescriptors,
      $ScopeObject, $Failure);
}

1;
