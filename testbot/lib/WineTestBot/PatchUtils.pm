# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
# Copyright 2018 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

package WineTestBot::PatchUtils;

=head1 NAME

WineTestBot::PatchUtils - Parse and analyze patches.

=head1 DESCRIPTION

Provides functions to parse patches and figure out which impact they have on
the Wine builds.

=cut

use Exporter 'import';
our @EXPORT = qw(GetPatchImpacts LastPartSeparator UpdateWineData
                 GetBuildTimeout GetTestTimeout);

use List::Util qw(min max);

use WineTestBot::Config;
use WineTestBot::Utils;


#
# Source repository maintenance
#

sub _Dir2ModuleName($$)
{
  my ($Root, $Dir) = @_;

  # Add a .exe extension to program directory names, but not if they already
  # contain some other extension.
  return "$Dir.exe" if ($Root eq "programs" and $Dir !~ /\./);

  # Dll directory names can normally be used as is, except if a '.dll'
  # extension was added because they contain dots.
  $Dir =~ s/\.dll$//;
  return $Dir;
}

=pod
=over 12

=item C<UpdateWineData()>

Updates information about the Wine source, such as the list of Wine files,
for use by the TestBot server.

=back
=cut

sub UpdateWineData($)
{
  my ($WineDir) = @_;

  mkdir "$DataDir/latest" if (!-d "$DataDir/latest");

  my $ErrMessage = `cd '$WineDir' && git ls-tree -r --name-only HEAD 2>&1 >'$DataDir/latest/winefiles.txt' && egrep '^PARENTSRC *=' dlls/*/Makefile.in programs/*/Makefile.in >'$DataDir/latest/wine-parentsrc.txt'`;
  return $? != 0 ? $ErrMessage : undef;
}

my $_TimeStamp;
my $_WineFiles;
my $_TestList;
my $_WineParentDirs;

=pod
=over 12

=item C<_LoadWineFiles()>

Reads latest/winefiles.txt to build a per-module hashtable of the test unit
files and a hashtable of all the Wine files.

=back
=cut

sub _LoadWineFiles()
{
  my $FileName = "$DataDir/latest/winefiles.txt";
  my $MTime = GetMTime($FileName);

  if ($_TestList and $_TimeStamp == $MTime)
  {
    # The file has not changed since we loaded it
    return;
  }

  $_TimeStamp = $MTime;
  $_TestList = {};
  $_WineFiles = {};
  if (open(my $fh, "<", $FileName))
  {
    while (my $Line = <$fh>)
    {
      chomp $Line;
      $_WineFiles->{$Line} = 1;

      if ($Line =~ m~^(dlls|programs)/([^/]+)/tests/([^/]+)$~)
      {
        my ($Root, $Module, $File) = ($1, $2, $3);
        next if ($File eq "testlist.c");
        next if ($File !~ /\.(?:c|spec)$/);
        $Module = _Dir2ModuleName($Root, $Module);
        $_TestList->{$Module}->{$File} = 1;
      }
    }
    close($fh);
  }

  $_WineParentDirs = {};
  $FileName = "$DataDir/latest/wine-parentsrc.txt";
  if (open(my $fh, "<", $FileName))
  {
    while (my $Line = <$fh>)
    {
      if ($Line =~ m~^\w+/([^/]+)/Makefile\.in:PARENTSRC *= *\.\./([^/\s]+)~)
      {
        my ($Child, $Parent) = ($1, $2);
        $_WineParentDirs->{$Parent}->{$Child} = 1;
      }
    }
    close($fh);
  }
}


#
# Wine patch analysis
#

# These paths are too generic to be proof that this is a Wine patch.
my $AmbiguousPathsRe = join('|',
  'Makefile\.in$',
  'aclocal\.m4$',
  'configure$',
  'configure\.ac$',
  'include/Makefile\.in$',
  'include/config\.h\.in$',
  'po/',
  'tools/Makefile.in',
  'tools/config.guess',
  'tools/config.sub',
  'tools/install-sh',
  'tools/makedep.c',
);

# Patches to these paths don't impact the Wine build. So ignore them.
my $IgnoredPathsRe = join('|',
  '\.mailmap$',
  'ANNOUNCE$',
  'AUTHORS$',
  'COPYING\.LIB$',
  'LICENSE\$',
  'LICENSE\.OLD$',
  'MAINTAINERS$',
  'README$',
  'VERSION$',
  'documentation/',
  'tools/c2man\.pl$',
  'tools/winapi/',
  'tools/winemaker/',
);

sub LastPartSeparator()
{
  return "===== TestBot: Last patchset part =====\n";
}

sub _CreateModuleInfo($$$)
{
  my ($Impacts, $Root, $Dir) = @_;

  my $ModuleName = _Dir2ModuleName($Root, $Dir);
  $Impacts->{IsWinePatch} = 1;

  my $Module = $Impacts->{Modules}->{$ModuleName};
  if (!$Module)
  {
    $Impacts->{Modules}->{$ModuleName} = $Module = {
      "Name"  => $ModuleName,
      "Path"    => "$Root/$Dir/tests",
      "ExeBase" => "${ModuleName}_test",
    };
    foreach my $File (keys %{$_TestList->{$ModuleName}})
    {
      $Module->{TestFiles}->{$File} = 0; # not modified
    }
  }

  return $Module;
}

sub _HandleFile($$$)
{
  my ($Impacts, $FilePath, $Change) = @_;

  if ($Change eq "new")
  {
    delete $Impacts->{DeletedFiles}->{$FilePath};
    $Impacts->{NewFiles}->{$FilePath} = 1;
  }
  elsif ($Change eq "rm")
  {
    delete $Impacts->{NewFiles}->{$FilePath};
    $Impacts->{DeletedFiles}->{$FilePath} = 1;
  }

  if ($FilePath =~ m~^(dlls|programs)/([^/]+)/tests/([^/\s]+)$~)
  {
    my ($Root, $Dir, $File) = ($1, $2, $3);

    my $Module = _CreateModuleInfo($Impacts, $Root, $Dir);
    $Module->{TestFiles}->{$File} = $Change;
    $Module->{PatchedTests} = 1;

    if ($File eq "Makefile.in" and $Change ne "modify")
    {
      # This adds / removes a directory
      $Impacts->{MakeMakefiles} = 1;
    }
  }
  elsif ($FilePath =~ m~^(dlls|programs)/([^/\s]+)/((?:[^\s]+/)?[^/\s]+)$~)
  {
    my ($Root, $PatchedDir, $File) = ($1, $2, $3);

    foreach my $Dir ($PatchedDir, keys %{$_WineParentDirs->{$PatchedDir} || {}})
    {
      my $Module = _CreateModuleInfo($Impacts, $Root, $Dir);
      $Module->{Patched} = 1;
    }

    if ($File eq "Makefile.in" and $Change ne "modify")
    {
      # This adds / removes a directory
      $Impacts->{MakeMakefiles} = 1;
    }
  }
  elsif ($Impacts->{WineFiles}->{$FilePath})
  {
    if ($FilePath !~ /^(?:$AmbiguousPathsRe)/)
    {
      $Impacts->{IsWinePatch} = 1;
    }
    # Else this file exists in Wine but has a very common name so it may just
    # as well belong to another repository.

    if ($FilePath !~ /^(?:$IgnoredPathsRe)/)
    {
      $Impacts->{BuildRoot} = $Impacts->{PatchedRoot} = 1;
      if ($FilePath =~ m~/Makefile\.in$~ and $Change ne "modify")
      {
        # This adds / removes a directory
        $Impacts->{MakeMakefiles} = 1;
      }
    }
    # Else patches to this file don't impact the Wine build.
  }
  elsif ($FilePath =~ m~/Makefile\.in$~ and $Change eq "new")
  {
    # This may or may not be a Wine patch but the new Makefile.in will be
    # added to the build by make_makefiles.
    $Impacts->{BuildRoot} = $Impacts->{PatchedRoot} = $Impacts->{MakeMakefiles} = 1;
  }
}

=pod
=over 12

=item C<GetPatchImpacts()>

Analyzes a patch and returns a hashtable describing the impact it has on the
Wine build: whether it requires updating the makefiles, re-running autoconf or
configure, whether it impacts the tests, etc.

Notes: A module is a dlls/ or programs/ directory. All references to patchsets
mean the patchset as being tested. So if a patch series has 5 parts and the
TestBot is testing part 3, the LAST PART is part 3, and ANY PART means part 1,
2 or 3.

=over
=item IsWinePatch

This flag is set if the LAST PART of the patchset has been identified as a
Wine patch.

=item BuildModules

This is a count of the number of modules where either the source or the tests
have been modified by ANY PART of the patchset.

=item BuildRoot

This flag is set if ANY PART of the patchset modified the Wine source
outside of a specific module in a way that impacts the build.

=item Autoconf
=item MakeErrors
=item MakeFir
=item MakeMakefiles
=item MakeOpenGL
=item MakeRequests
=item MakeUnicode
=item MakeVulkan

These flags are set if ANY PART of the patchset requires running the
corresponding script before building Wine.

=item TestBuild

This flag is set if the LAST PART of the patchset modified the Wine source
such that the build needs to be tested.

=item RunTestUnits
=item PatchedModules
=item PatchedModuleOrTests

These are counts based on the LAST PART of the patchset, so excluding any
impact from PREVIOUS PARTS. They count, respectively, the test units that need
to be rerun (so excluding deleted ones), the modules whose source has been
modified (including file deletions), and the modules where either the source
or the tests have been modified (again including deletions).

=item PatchedRoot

This flag is set if the LAST PART of the patchset modified the Wine source
outside of a specific module.

=item Modules

A set of structures containing information about each module impacted by
ANY PART of the patchset, indexed by module name. Each module has the
following information:

=over

=item Patched

This flag is true if the LAST PART of the patchset has modified the module
itself, so excluding its test units.

=item PatchedTests

This flag is true if the LAST PART of the patchset has modified the tests of
the module.

=item TestUnits

This is an alphabetical list of the module's test units, after applying the
patchset (so excluding any test unit deleted by the patchset).

=item All

This flag is true if all the module's test units are impacted by the LAST PART
of the patchset. This is typically true if a resource file or helper dll is
modified.

=item RunTestUnits

This is an alphabetical list of the module's test units that are impacted by
the LAST PART of the patchset such that they should be rerun. In particular if
All is true this is identical to TestUnits.

=item TestFiles

This hashtable is indexed by the filenames of the files in the module's tests
directory. Note that this is not limited to the test unit C files but also
includes the resource files, makefile and helper dlls (but not testlist.c).
For each file the value describes the modification performed by the
LAST PART of the patchset: 0 (false) if there is no modification, "modify" for
modified files, "new" for added files and "rm" for deletions.

=back

=item WineFiles

The keys of this hashtable enumerate all the Wine source files, including all
files added and removed by PREVIOUS PARTS of the patchset.
This is mostly for the internal use of the impact analyzer and should be
treated as read-only by outside code (and copy-on-write by the analyzer).

=item NewFiles
=item DeletedFiles

The keys of these hashtables enumerate, respectively, the files added and
removed by the LAST PART of the patchset.
This is mostly for the internal use of the impact analyzer.

=back

=back
=cut

sub GetPatchImpacts($)
{
  my ($PatchFileName) = @_;

  my $fh;
  return undef if (!open($fh, "<", $PatchFileName));

  _LoadWineFiles();

  my $Impacts = { Modules => {}, WineFiles => $_WineFiles };
  my ($Path, $Change);
  while (my $Line = <$fh>)
  {
    # All the files on '---' lines will be passed to _HandleFile() when the
    # corresponding '+++' line comes.
    if ($Line =~ m=^--- \w+/(aclocal\.m4|configure\.ac)$=)
    {
      $Path = $1;
      $Impacts->{BuildRoot} = $Impacts->{Autoconf} = 1;
      $Impacts->{PatchedRoot} = 1;
    }
    elsif ($Line =~ m=^--- \w+/(tools/make_makefiles)$=)
    {
      $Path = $1;
      $Impacts->{BuildRoot} = $Impacts->{MakeMakefiles} = 1;
      $Impacts->{PatchedRoot} = $Impacts->{IsWinePatch} = 1;
    }
    elsif ($Line =~ m=^--- \w+/(tools/make_requests|server/protocol\.def)$=)
    {
      $Path = $1;
      $Impacts->{BuildRoot} = $Impacts->{MakeRequests} = 1;
      $Impacts->{PatchedRoot} = $Impacts->{IsWinePatch} = 1;
      # Note that make_requests essentially impacts every test but these
      # indirect impacts are ignored (use test=all if it really matters).
    }
    elsif ($Line =~ m=^--- \w+/(dlls/dsound/make_fir)$=)
    {
      $Path = $1;
      $Impacts->{MakeFir} = 1;
      $Impacts->{IsWinePatch} = 1;
    }
    elsif ($Line =~ m=^--- \w+/(dlls/ntdll/make_errors|dlls/ntdll/error\.c|include/ntstatus\.h|include/winerror\.h)$=)
    {
      $Path = $1;
      $Impacts->{MakeErrors} = 1;
      $Impacts->{IsWinePatch} = 1;
    }
    elsif ($Line =~ m=^--- \w+/(dlls/opengl32/make_opengl|dlls/opengl32/winegl\.xml|include/wine/wgl_driver\.h)$=)
    {
      $Path = $1;
      $Impacts->{BuildRoot} = $Impacts->{MakeOpenGL} = 1;
      $Impacts->{PatchedRoot} = $Impacts->{IsWinePatch} = 1;
      # Note that make_opengl impacts other dlls through header changes so
      # it could make sense for the TestBot to rerun the corresponding tests
      # when test=module. But that would require either hardcoding the list
      # of impacted modules, or detecting changed source files (and thus the
      # list of tests to run) at build time which is incompatible with the
      # current architecture.
      # So these indirect impacts are ignored for now. A workaround is to
      # use test=all.
    }
    elsif ($Line =~ m=^--- \w+/(dlls/winevulkan/make_vulkan)$=)
    {
      $Path = $1;
      $Impacts->{BuildRoot} = $Impacts->{MakeVulkan} = 1;
      $Impacts->{PatchedRoot} = $Impacts->{IsWinePatch} = 1;
      # Note that make_vulkan impacts other dlls through header changes.
      # See the make_opengl comment.
    }
    elsif ($Line =~ m=^--- \w+/(tools/make_unicode)$=)
    {
      $Path = $1;
      $Impacts->{BuildRoot} = $Impacts->{MakeUnicode} = 1;
      $Impacts->{PatchedRoot} = $Impacts->{IsWinePatch} = 1;
      # Note that make_unicode changes the source for many dlls.
      # See the make_opengl comment.
    }
    elsif ($Line =~ m=^--- /dev/null$=)
    {
      $Change = "new";
    }
    elsif ($Line =~ m~^--- \w+/([^\s]+)$~)
    {
      $Path = $1;
    }
    elsif ($Line =~ m~^\+\+\+ /dev/null$~)
    {
      _HandleFile($Impacts, $Path, "rm") if (defined $Path);
      $Path = undef;
      $Change = "";
    }
    elsif ($Line =~ m~^\+\+\+ \w+/([^\s]+)$~)
    {
      my $PlusPath = $1;
      _HandleFile($Impacts, $Path, "rm") if (defined $Path and $Path ne $PlusPath);
      _HandleFile($Impacts, $PlusPath, $Change || "modify");
      $Path = undef;
      $Change = "";
    }
    elsif ($Line eq LastPartSeparator())
    {
      # All the diffs so far belong to previous parts of this patchset.
      # But only the last part must be taken into account to determine if
      # testing is needed. So reset all the Patched* fields.

      delete $Impacts->{PatchedRoot};
      foreach my $Module (values %{$Impacts->{Modules}})
      {
        delete $Module->{Patched};
        delete $Module->{PatchedTests};
      }

      # Make a copy of the Wine files list reflecting the current situation.
      $Impacts->{WineFiles} = { %{$Impacts->{WineFiles}} };
      if ($Impacts->{IsWinePatch} or $Impacts->{MakeMakefiles})
      {
        map { $Impacts->{WineFiles}->{$_} = 1 } keys %{$Impacts->{NewFiles}};
        map { delete $Impacts->{WineFiles}->{$_} } keys %{$Impacts->{DeletedFiles}};
        $Impacts->{NewFiles} = {};
        $Impacts->{DeletedFiles} = {};
        delete $Impacts->{IsWinePatch};
      }

      # Reset the status of all modified test unit files to not modified
      # (deleted files won't come back).
      foreach my $Module (values %{$Impacts->{Modules}})
      {
        my $TestFiles = $Module->{TestFiles};
        foreach my $File (keys %$TestFiles)
        {
          if ($TestFiles->{$File} eq "rm")
          {
            delete $TestFiles->{$File};
          }
          else
          {
            $TestFiles->{$File} = 0;
          }
        }
      }
    }
    else
    {
      $Path = undef;
      $Change = "";
    }
  }
  close($fh);

  # Calculate the impact counts and lists of test units.
  foreach my $Module (values %{$Impacts->{Modules}})
  {
    $Module->{TestUnits} = [];
    $Module->{RunTestUnits} = [];

    # For each module build the lists of test units
    my $TestFiles = $Module->{TestFiles};
    foreach my $File (sort keys %$TestFiles)
    {
      my $Base = $File;
      if ($Base !~ s/(?:\.c|\.spec)$//)
      {
        # Any change to a non-C non-Spec file (including deletions) can
        # potentially impact all tests
        $Module->{All} = 1 if ($TestFiles->{$File});
        next;
      }

      if (exists $TestFiles->{"$Base.spec"})
      {
        # This is a helper dll, not a test unit.
        # Any change, including deletions, could impact all tests.
        $Module->{All} ||= ($TestFiles->{"$Base.c"} or
                            $TestFiles->{"$Base.spec"});
        next;
      }

      if ($TestFiles->{$File} ne "rm")
      {
        push @{$Module->{TestUnits}}, $Base;
        push @{$Module->{RunTestUnits}}, $Base if ($TestFiles->{$File});
      }
    }
    $Module->{All} ||= (@{$Module->{TestUnits}} == @{$Module->{RunTestUnits}});
    $Module->{RunTestUnits} = $Module->{TestUnits} if ($Module->{All});

    my $RunTestUnits = scalar(@{$Module->{RunTestUnits}});
    $Impacts->{RunTestUnits} += $RunTestUnits;
    $Impacts->{ModuleTestUnits} += $Module->{Patched} ? scalar(@{$Module->{TestUnits}}) : $RunTestUnits;

    if ($Module->{Patched} or $Module->{PatchedTests})
    {
      $Impacts->{PatchedModuleOrTests}++;
      $Impacts->{PatchedModules}++ if ($Module->{Patched});
    }
    $Impacts->{BuildModules}++;
  }
  $Impacts->{TestBuild} = $Impacts->{PatchedRoot} || $Impacts->{PatchedModuleOrTests};

  return $Impacts;
}


#
# Compute task timeouts based on the patch data
#

sub GetBuildTimeout($$)
{
  my ($Impacts, $TaskMissions) = @_;

  my ($ExeCount, $WineCount);
  map { $_ =~ /^exe/ ? $ExeCount++ : $WineCount++ } keys %{$TaskMissions->{Builds}};

  # Set $ModuleCount to 0 if a full rebuild is needed
  my $ModuleCount = (!$Impacts or $Impacts->{BuildRoot}) ? 0 :
                    $Impacts->{BuildModules};
  my $Reconfig = (!$Impacts or $Impacts->{Autoconf} or $Impacts->{MakeMakefiles});

  my ($ExeTimeout, $WineTimeout) = (0, 0);
  if ($ExeCount)
  {
    my $OneBuild = $ModuleCount ? $ModuleCount * $ExeModuleTimeout :
                                  $ExeBuildTimeout;
    $ExeTimeout = $ExeCount * min($ExeBuildTimeout, $OneBuild) +
                  ($Reconfig ? $ExeCount * $ReconfigBuildTimeout : 0);
  }
  if ($WineCount)
  {
    my $OneBuild = $ModuleCount ? $ModuleCount * $WineModuleTimeout :
                                  $WineBuildTimeout;
    $WineTimeout = $WineCount * min($WineBuildTimeout, $OneBuild) +
                   ($Reconfig ? $WineCount * $ReconfigBuildTimeout : 0);
  }

  return $ExeTimeout + $WineTimeout;
}

sub GetTestTimeout($$)
{
  my ($Impacts, $TaskMissions) = @_;

  my $Timeout = 0;
  foreach my $Mission (@{$TaskMissions->{Missions}})
  {
    if (!$Impacts or ($Mission->{test} eq "all" and
                      ($Impacts->{PatchedRoot} or $Impacts->{PatchedModules})))
    {
      $Timeout += $SuiteTimeout;
    }
    elsif ($Mission->{test} ne "build")
    {
      # Note: If only test units have been patched then
      #       ModuleTestUnits == RunTestUnits.
      my $UnitCount = $Mission->{test} eq "test" ? $Impacts->{RunTestUnits} :
                                                   $Impacts->{ModuleTestUnits};
      my $TestsTimeout = min(2, $UnitCount) * $SingleTimeout +
                         max(0, $UnitCount - 2) * $SingleAvgTime;
      $Timeout += min($SuiteTimeout, $TestsTimeout);
    }
  }
  return $Timeout;
}

1;
