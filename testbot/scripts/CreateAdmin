#!/usr/bin/perl -Tw
# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
#
# Creates an administrator user for the initial setup.
#
# Copyright 2022 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

sub BEGIN
{
  if ($0 !~ m=^/=)
  {
    # Turn $0 into an absolute path so it can safely be used in @INC
    require Cwd;
    $0 = Cwd::cwd() . "/$0";
  }
  if ($0 =~ m=^(/.*)/[^/]+/[^/]+$=)
  {
    $::RootDir = $1;
    unshift @INC, "$::RootDir/lib";
  }
}
my $Name0 = $0;
$Name0 =~ s+^.*/++;


use WineTestBot::Roles;
use WineTestBot::Users;
use WineTestBot::Log;
use WineTestBot::Utils;


#
# Logging and error handling helpers
#

sub Error(@)
{
  print STDERR "$Name0:error: ", @_;
  LogMsg @_;
}


#
# Setup and command line processing
#

my ($OptUserName, $Usage);
while (@ARGV)
{
  my $Arg = shift @ARGV;
  if ($Arg =~ /^(?:-\?|-h|--help)$/)
  {
    $Usage = 0;
    last;
  }
  elsif ($Arg =~ /^-/)
  {
    Error "unknown option '$Arg'\n";
    $Usage = 2;
    last;
  }
  elsif (!defined $OptUserName)
  {
    $OptUserName = $Arg;
  }
  else
  {
    Error "unexpected argument '$Arg'\n";
    $Usage = 2;
    last;
  }
}

# Check parameters
if (!defined $Usage)
{
  if (!defined $OptUserName)
  {
    Error "You must specify the user name\n";
    $Usage = 2;
  }
}
if (defined $Usage)
{
  print "Usage: $Name0 [--help] USERNAME\n";
  print "\n";
  print "Creates an administrator account. Instructions for logging in will be provided on stdout.\n";
  print "\n";
  print "Where:\n";
  print "  USERNAME      The administrator account name.\n";
  print "  --help        Shows this usage message.\n";
  exit $Usage;
}


#
# Create the administrator
#

my $Users = CreateUsers();
my $User = $Users->Add();
$User->Name($OptUserName);
$User->EMail("$OptUserName\@localhost");
my $ResetCode = GenerateRandomString(32);
$User->ResetCode($ResetCode);
$User->AddDefaultRoles();
$User->AddRole(CreateRoles()->GetItem("admin"));

NotifyAdministrator("Creating an administrator account for $OptUserName",
                    "Creating an administrator account for $OptUserName.\n");

my ($_ErrKey, $ErrProperty, $ErrMessage) = $Users->Save();
if (defined $ErrMessage)
{
  Error "Could not create the $OptUserName user: $ErrMessage\n";
  exit 1;
}


my $URL = MakeOfficialURL("/ResetPassword.pl");
print <<EOF;
You can now log in using the following information
URL:       $URL
Username:  $OptUserName
ResetCode: $ResetCode

Then use your fresh administrator powers to set your email address,
your real name, and adjust your roles as appropriate.
EOF

exit 0;
