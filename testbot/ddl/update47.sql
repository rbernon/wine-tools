USE winetestbot;

CREATE TABLE Failures
(
  Id             INT NOT NULL AUTO_INCREMENT,
  ErrorGroup     VARCHAR(64) NULL,
  TestUnit       VARCHAR(32) NULL,
  ConfigRegExp   VARCHAR(64) NULL,
  FailureRegExp  VARCHAR(256) NOT NULL,
  Notes          VARCHAR(128) NULL,
  LastNew        DATETIME NULL,
  LastOld        DATETIME NULL,
  BugId          INT NOT NULL,
  BugStatus      VARCHAR(32) NULL,
  BugDescription VARCHAR(128) NULL,
  PRIMARY KEY (Id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE TaskFailures
(
  FailureId      INT         NOT NULL,
  JobId          INT         NOT NULL,
  StepNo         INT(2)      NOT NULL,
  TaskNo         INT(2)      NOT NULL,
  TaskLog        VARCHAR(32) NOT NULL,
  NewCount       INT         NULL,
  OldCount       INT         NULL,
  FOREIGN KEY(FailureId) REFERENCES Failures(Id),
  FOREIGN KEY(JobId, StepNo, TaskNo) REFERENCES Tasks(JobId, StepNo, No),
  PRIMARY KEY (FailureId, JobId, StepNo, TaskNo, TaskLog)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;
