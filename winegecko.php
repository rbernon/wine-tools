<?php
/**
 * Redirects to one of many URLs that have the Wine Gecko installer available.
 * 
 * Usage: 
 * winegecko.php
 * (main usage, redirects to one of many URLs that have the Wine Gecko installer available)
 * 
 * winegecko.php?action=showlist
 * (display a list of server and tells if the file is available for each server)
 * 
 * Copyright (c) 2006 Jonathan Ernst
 */

// Default version if none given
$sVersion = '0.0.1';

// Suffix appended to base name of file
$sFileSuffix = '';

// Folder which contains wine gecko files
$sFolder = 'wine-gecko';

// Check if a specific version was passed
if(isset($_GET['v'])) {
	$sVersion = $_GET['v'];

	if(isset($_GET['arch']))
		$sFileSuffix = $sVersion.'-'.$_GET['arch'];
}

if(!$sFileSuffix)
	$sFileSuffix = $sVersion;

$sFile = 'wine-gecko';
$sExt = 'msi';

switch($sVersion) {
case '0.0.1':
case '0.1.0':
case '0.9.0':
case '0.9.1':
case '1.0.0':
case '1.1.0':
     $sExt = 'cab';
     /* fall through */
case '1.2.0':
case '1.3':
case '1.4':
case '1.5':
case '1.6':
case '1.7':
case '1.8':
case '1.9':
case '2.21':
case '2.24':
case '2.34':
case '2.36':
case '2.40':
case '2.44':
case '2.47':
     $sFile = 'wine_gecko';
     break;
}

// Name of the file
$sFileName = sprintf('%s/%s/%s-%s.%s', $sFolder, $sVersion, $sFile, $sFileSuffix, $sExt);

// Common code for Wine downloader scripts
require("download.inc.php");
?>
